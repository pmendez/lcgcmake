set (outscript ${TARGET})

if (EXISTS ${append_template_name})
  file (READ ${append_template_name} append_template_content)
  file (APPEND ${outscript} "${append_template_content}")
endif()

message("Write environment scripts to ${outscript}")
